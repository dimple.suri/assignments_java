package day3;

public class Student {
	private int studentid;
	private String studentname;
	private int score;
	public void setStudentid(int studentid)
	{
		this.studentid=studentid;//class level variable will be initialized with parameter
	}
	public int getStudentid()
	{
		return studentid;
		
	}
	public void setStudentname(String studentname)
	{
		this.studentname=studentname;
	}
	public String getStudentname()
	{
		return this.studentname;
	}
	public void setScore(int score)
	{
		this.score=score;
	}
	public int getScore()
	{
		return this.score;
	}
	

}
