package day2;
import java.util.*;
public class Calculator {
	int number1;
	int number2;
	Scanner scobj;
	//Scanner class can be used to take any input from user.
	//void-return type
	public void accept()
	{
		scobj=new Scanner(System.in);//I am initializing scanner object
		//that it should take value from keyboard
		System.out.println("Enter value for number 1");
		number1=scobj.nextInt();
		System.out.println("Enter value for number 2");
		number2=scobj.nextInt();
		
	}
	public void add()
	{
		int total=number1+number2;
		System.out.println("Total is "+total);
	}
	public void subtract()
	{
		int subtract=number1-number2;
		System.out.println("Subtraction is "+subtract);
		System.out.println("This is subtract method");
	}
	public void multipication()
	{
		System.out.println("This is multipication method");
	}
	public static void main(String[] args) {
		Calculator calobj;//Reference variable
		calobj=new Calculator();//this is an object and it is initializing memory here
		calobj.accept();
		
		calobj.add();
		calobj.subtract();
		calobj.multipication();
		Calculator calobj1=new Calculator();
		calobj1.add();
		calobj1.subtract();
	}

}
