package day1;
//in any java file package statement should be first statement.

public class HelloWorld {
	/*
	 * public-this method can be access outside jvm also
	 * static-can be accessed without any object also
	 * void-return type
	 * main-method name.
	 * 
	 */
	public static void main(String arg[])
	{

		byte age;//Declaration of variable
		age=34;//Assignment to variable.
		short score=156;
		int empid=10001;
		long mobileno=1324323;
		float percentage=89.34F;
		double salary=324323.3333;
		boolean choice=true;
		String empname="Anaita";
		
		System.out.println("Age is "+age);
		System.out.println("Score is "+score);
		System.out.println("Empid is "+empid);
		System.out.println("Percentage is "+percentage);
		System.out.println("Employee name is "+empname);
		
		
	}

}
